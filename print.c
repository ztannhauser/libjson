#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "libavl.h"
#include "libdebug.h"
#include "libarray.h"
#include "libcstring.h"

#include "structs.h"

void json_print(
	struct json* json,
	void* ptr,
	void (*push)(void* ptr, char* c, int n)
)
{
	int depth = 0;
	void go(struct json* json, bool ident)
	{
		switch(json->kind)
		{
			case jk_boolean:
			{
				struct json_boolean* spef = json;
				if(ident)
				{
					for(int i = depth;i--;)
					{
						push(ptr, "\t", 1);
					}
				}
				if(spef->value)
				{
					push(ptr, "true", 4);
				}
				else
				{
					push(ptr, "false", 5);
				}
				break;
			}
			case jk_number:
			{
				struct json_number* spef = json;
				if(ident)
				{
					for(int i = depth;i--;)
					{
						push(ptr, "\t", 1);
					}
				}
				char buffer[32];
				int len;
				if(spef->is_double)
				{
					len = sprintf(buffer, "%f", spef->decimal);
				}
				else
				{
					len = sprintf(buffer, "%li", spef->integer);
				}
				push(ptr, buffer, len);
				break;
			}
			case jk_string:
			{
				struct json_string* spef = json;
				if(ident)
				{
					for(int i = depth;i--;)
					{
						push(ptr, "\t", 1);
					}
				}
				cstring_print(&(spef->chars), ptr, push, true, true);
				break;
			}
			case jk_list:
			{
				struct json_list* spef = json;
				if(ident)
				{
					for(int i = depth;i--;)
					{
						push(ptr, "\t", 1);
					}
				}
				push(ptr, "[\n", 2);
				depth++;
				for(int i = 0, n = spef->elements.n;i < n;)
				{
					struct json_object* ele =
						array_index(spef->elements, i, struct json_object*);
					go(ele, true);
					if(++i < n)
					{
						push(ptr, ",", 1);
					}
					push(ptr, "\n", 1);
				}
				depth--;
				for(int i = depth;i--;)
				{
					push(ptr, "\t", 1);
				}
				push(ptr, "]", 1);
				break;
			}
			case jk_object:
			{
				struct json_object* spef = json;
				if(ident)
				{
					for(int i = depth;i--;)
					{
						push(ptr, "\t", 1);
					}
				}
				push(ptr, "{\n", 2);
				depth++;
				for(
					struct avl_node* cursor = spef->values.leftmost;
					cursor;
					cursor = cursor->next
				)
				{
					struct json_object_value* ele = cursor->item;
					for(int i = depth;i--;)
					{
						push(ptr, "\t", 1);
					}
					cstring_print(&(ele->name),
						ptr, push, true, true);
					push(ptr, ": ", 2);
					go(ele->value, false);
					if(cursor->next)
					{
						push(ptr, ",", 1);
					}
					push(ptr, "\n", 1);
				}
				depth--;
				for(int i = depth;i--;)
				{
					push(ptr, "\t", 1);
				}
				push(ptr, "}", 1);
				break;
			}
			default: TODO;
		}
	}
	go(json, true);
	push(ptr, "\n", 1);
}


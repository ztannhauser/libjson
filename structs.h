#include <stdbool.h>

enum json_kind
{
	jk_number,
	jk_boolean,
	jk_string,
	jk_list,
	jk_object,
	jk_n,
};

struct json
{
	enum json_kind kind;
};

struct json_number
{
	struct json super;
	bool is_double;
	union
	{
		double decimal;
		signed long integer;
	};
};

struct json_boolean
{
	struct json super;
	bool value;
};

struct json_string
{
	struct json super;
	struct array chars;
};

struct json_list
{
	struct json super;
	struct array elements; // elements are struct json*
};

struct json_object
{
	struct json super;
	struct avl_tree values;
};

struct json_object_value
{
	struct array name;
	struct json* value;
};


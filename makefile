MAKEFLAGS += --no-print-directory

.PHONY: default
.PHONY: installdeps
.PHONY: both

default: main.a

both: installdeps main.a

include arch.mk
include install.mk
-include .dir.mk

deps += ../libarray
deps += ../libchar
deps += ../libavl
deps += ../libcstring
deps += ../libdebug

deps_a = $(foreach dep,$(deps),$(dep).a)

.%.c.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ || ($(EDITOR) $< && false)
.%.cpp.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@
.dir.o:
	$(LD) -r $^ -o $@
%/.dir.o:
	$(LD) -r $^ -o $@

main.a: .dir.o
	$(AR) -rsc main.a .dir.o

installdeps: cmd = $(MAKE) -C $(dep) install &&
installdeps:
	$(foreach dep,$(deps),$(cmd)) true

clean:
	find . -type f -name '*.o' | xargs rm -vf main

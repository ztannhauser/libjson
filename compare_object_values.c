#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libavl.h"
#include "libarray.h"
#include "libdebug.h"

#include "structs.h"

int compare_object_values(
	struct json_object_value* a,
	struct json_object_value* b)
{
	return strcmp(a->name.data, b->name.data);
}


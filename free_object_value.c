#include <stdio.h>
#include <stdlib.h>

#define DEBUG 0
#include "libarray.h"
#include "libdebug.h"
#include "libavl.h"

#include "./structs.h"
#include "./free.h"

void free_object_value(struct json_object_value* value)
{
	ENTER;
	array_delete(&(value->name));
	json_free(value->value);
	free(value);
	EXIT;
}


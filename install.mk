projectname = $(shell basename $(shell pwd))

install: installdeps ../$(projectname).a  ../$(projectname).h

../$(projectname).a: both
	cp main.a ../$(projectname).a

../$(projectname).h: public.h
	cp public.h ../$(projectname).h

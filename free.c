#include <stdio.h>
#include <stdlib.h>

#define DEBUG 0
#include "libarray.h"
#include "libdebug.h"
#include "libavl.h"

#include "./structs.h"

void json_free(struct json* json)
{
	ENTER;
	verpv(json->kind);
	switch(json->kind)
	{
		case jk_number: break;
		case jk_boolean: break;
		case jk_string:
		{
			struct json_string* spef = json;
			array_delete(&(spef->chars));
			break;
		}
		case jk_list:
		{
			struct json_list* spef = json;
			for(int i = 0, n = spef->elements.n;i < n;i++)
			{
				struct json* ele = array_index(spef->elements, i, void*);
				json_free(ele);
			}
			array_delete(&(spef->elements));
			break;
		}
		case jk_object:
		{
			struct json_object* spef = json;
			avl_free_nodes(&(spef->values));
			break;
		};
		default: TODO;
	}
	free(json);
	EXIT;
}








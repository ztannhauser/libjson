#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define DEBUG 0
#include "libarray.h"
#include "libdebug.h"
#include "libchar.h"
#include "libcstring.h"
#include "libavl.h"

#include "./structs.h"
#include "./compare_object_values.h"
#include "./free_object_value.h"

struct json* json_scan(void* ptr, int (*next)(void* ptr, char* c), char* c)
{
	ENTER;
	verpv(ptr);
	verpv(next);
	verpv(c);
	enum token
	{
		t_eof = 256,
		t_true,
		t_false,
		t_string,
		t_integer,
		t_decimal,
		t_n,
	};
	enum token current;
	struct array string;
	double decimal;
	signed long integer;
	void next_token()
	{
		ENTER;
		verpvc(*c);
		while(
			(*c == ' ') ||
			(*c == '\n') ||
			(*c == '\t')
		)
		{
			verprintf("skipping whitespace...\n");
			if(!next(ptr, c))
			{
				current = t_eof;
				return;
			}
		}
		verpvc(*c);
		switch(*c)
		{
			case '0':
			case '1' ... '9':
			{
				char buffer[30], *m = buffer;
				do
				{
					*m++ = *c;
					next(ptr, c);
				}
				while(is_num(*c));
				*m++ = '\0';
				verpvs(buffer);
				integer = strtol(buffer, &m, 0);
				if(*m)
				{
					decimal = strtod(buffer, &m);
					if(*m)
					{
						assert(!"unparseable number!");
					}
					else
					{
						current = t_decimal;
					}
				}
				else
				{
					current = t_integer;
				}
				break;
			}
			case '\"':
			{
				string = cstring_scan(ptr, next, c, true);
				current = t_string;
				break;
			}
			case ':':
			case ',':
			case '[': case ']':
			case '{': case '}':
			{
				current = *c;
				next(ptr, c);
				break;
			}
			case 't':
			{
				assert(next(ptr, c) && (*c == 'r'));
				assert(next(ptr, c) && (*c == 'u'));
				assert(next(ptr, c) && (*c == 'e'));
				assert(next(ptr, c));
				current = t_true;
				break;
			}
			case 'f':
			{
				assert(next(ptr, c) && (*c == 'a'));
				assert(next(ptr, c) && (*c == 'l'));
				assert(next(ptr, c) && (*c == 's'));
				assert(next(ptr, c) && (*c == 'e'));
				assert(next(ptr, c));
				current = t_false;
				break;
			}
			default: TODO;
		}
		EXIT;
	}
	next_token();
	verpv(current);
	struct json* parse()
	{
		ENTER;
		struct json* ret;
		switch(current)
		{
			case t_integer:
			{
				struct json_number* spef = ret =
					malloc(sizeof(struct json_number));
				ret->kind = jk_number;
				spef->is_double = false;
				spef->integer = integer;
				next_token();
				break;
			}
			case t_decimal:
			{
				struct json_number* spef = ret =
					malloc(sizeof(struct json_number));
				ret->kind = jk_number;
				spef->is_double = true;
				spef->decimal = decimal;
				next_token();
				break;
			}
			case t_true:
			{
				struct json_boolean* spef = ret =
					malloc(sizeof(struct json_boolean));
				ret->kind = jk_boolean;
				spef->value = true;
				next_token();
				break;
			}
			case t_false:
			{
				struct json_boolean* spef = ret =
					malloc(sizeof(struct json_boolean));
				ret->kind = jk_boolean;
				spef->value = false;
				next_token();
				break;
			}
			case t_string:
			{
				struct json_string* spef = ret =
					malloc(sizeof(struct json_string));
				ret->kind = jk_string;
				spef->chars = string;
				next_token();
				break;
			};
			case '[':
			{
				struct json_list* spef = ret = malloc(sizeof(struct json_list));
				ret->kind = jk_list;
				spef->elements = new_array(struct json*);
				next_token();
				while(current != ']')
				{
					struct json* sub = parse();
					array_push_n(&(spef->elements), &sub);
					if(current == ',')
					{
						next_token();
					}
				}
				next_token();
				break;
			}
			case '{':
			{
				struct json_object* spef = ret =
					malloc(sizeof(struct json_object));
				ret->kind = jk_object;
				avl_init_tree(
					&spef->values,
					compare_object_values,
					free_object_value);
				next_token(); // eat '{'
				while(current != '}')
				{
					struct json_object_value* new = 
						malloc(sizeof(struct json_object_value));
					assert(current == t_string);
					new->name = string;
					next_token(); // eat string
					next_token(); // eat colon
					new->value = parse();
					avl_insert(&spef->values, new);
					if(current == ',')
					{
						next_token();
					}
				}
				next_token(); // eat '}'
				break;
			};
			default:
			{
				TODO;
			}
		}
		EXIT;
		return ret;
	}
	struct json* ret = parse();
	verpv(ret);
	EXIT;
	return ret;
}
























